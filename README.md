# React Tools
A Project generate document from base64

## Usage
* Go to http://huynh.dn.pages.temando.io/tools
* Paste base64 code in the form input and click submit

## Installing
NodeJS and NPM is required

1. Clone this repo ``git clone git@src.temando.io:huynh.dn/tools.git``.
2. Run ``npm install`` to get all the Node Packages.
3. Run ``npm start`` for development mode.
4. New browser window should open automatically.

## Linting

```javascript
npm run lint
```

## Building

```javascript
npm run build
```

Demo: 

## Built With

* [Webpack 4](https://webpack.js.org/) - A module bundler.
* [Babel](https://babeljs.io/) - A JavaScript compiler.
* [Eslint](https://eslint.org/) - The pluggable linting utility for JavaScript and JSX.

#### Style using SASS / CSS

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request.

## Author
[HuynhDN.](https://src.temando.io/huynh.dn/)




